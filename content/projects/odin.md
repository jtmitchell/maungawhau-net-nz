+++
title = "Odin Solutions"
date = "2016-08-12"
[taxonomies]
tags = ["odin","website","php","angular","nodejs",]
[extra]
client = "Odin Solutions"
start = "2015-10-01"
end = "2016-08-01"
image = "/projects/odin-thl-winners.crop.jpg"
image_caption = "UK Health Lottery project"
+++

In just under a year with **Odin Solutions**(http://www.odinsol.com/)
we worked on two large client-facing projects and an internal product.
We worked closely with the New Zealand team to standardise practices in
automated testing, Jenkins CI deployments, and code delivery.

<!-- more -->

The first project was to customise the open source [Revive Adserver](https://www.revive-adserver.com/)
to capture and report on the metrics they required for campaigns, and to simplify the data entry
for entering their ads.

The other major project was to create a brand-new site for the [UK Health Lottery](https://www.healthlottery.co.uk/).

![Health Lottery homepage](/projects/odin-thl-home.jpg "Homepage")

This involved writing a bespoke CMS backend in Node.js to serve the whole site,
and AngularJS components to add interactivity to pages by calling the server API.

![List of CMS content](/projects/odin-thl-winners.jpg "List of CMS content")

CMS content was required for articles about recent winners and news about the causes supported by the lottery.

![Lookup results](/projects/odin-thl-results.jpg "Lookup results")

The AngularJS components handled interactive tasks, such as the retrieval and display of results,
and the ability to find nearby retailers.

![Find nearby retailers](/projects/odin-thl-retailers.crop.jpg "Find nearby retailers")
