+++
title = "Shopping at Ferrit"
description = "Telecom NZ's shopping portal"
date = "2009-03-01"
[taxonomies]
tags = ["telecom","website","ecommerce","perl","virtual_machine",]
[extra]
client = "Telecom NZ"
start = "2006-10-01"
end = "2009-02-01"
image = "/projects/ferrit-cameras.png"
image_caption = "Ferrit"
+++


We worked on Telecom's Ferrit online shopping site to deliver new capabilities, such as search and direct bank deposit payments,
and to support the huge Perl-based application.
<!-- more -->
![Ferrit](/projects/ferrit-cameras.png)

Part of the support was to streamline the deployments onto the production servers, and to create a solution using Virtual Machines
for staff to run their own test versions of the whole site.
