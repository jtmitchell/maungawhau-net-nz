+++
title = "Network Monitoring"
date = "2002-08-01"
[taxonomies]
tags = ["unl","linux","network_monitoring",]
[extra]
client = "United Networks Ltd."
start = "2002-03-01"
end = "2002-05-01"
+++

We migrated network monitoring software from a trial
laptop setup running on NT, to a full production setup on Linux, providing
scalability, ease of maintenance, and improved customer reporting.
<!-- more -->

The project involved installation of new hardware and software, redeployment
of existing collection data, writing scripts to integrate the monitoring tool
with the UNL infrastructure, and providing consultation on future directions
for emergency recovery and web-enabling the customer reports.
