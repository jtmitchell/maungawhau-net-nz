+++
title = ".NET meets DNS"
description = "Webservices on Linux to manipulate DNS"
date = "2006-03-01"
[taxonomies]
tags = ["woosh",".net","linux","dns",]
[extra]
client = "Woosh Wireless"
start = "2005-11-01"
end = "2006-02-01"
+++


We developed an interface between the Woosh .NET environment and backend
DNS services running on Linux. 

<!-- more -->

The interface was written in C# and ran on Mono,
providing single uniform access to three products on the Unix
backend.

This allowed Woosh to integrate them as a single product on their
self-service website.
