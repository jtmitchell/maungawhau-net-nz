+++
title = "TelstraClear"
description = "Logging and searching log data from switches"
date = "2005-10-01"
[taxonomies]
tags = ["telstraclear","perl","linux","network_monitoring","dns","soap",]
[extra]
client = "TelstraClear"
start = "2004-06-01"
end = "2005-09-01"
+++

We worked with the Auckland Operations group to update a log capture and
analysis tool to provide greater flexibility and to run on Solaris.
<!-- more -->

We also developed a web-based data forms tool to store advisory notices from
vendors, or any other arbitrary textual data; and developed an
infrastructure to perform tape backups across the network to any
tape-enabled workstation.
