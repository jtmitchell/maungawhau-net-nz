+++
title = "StellarLibrary"
description = "Making it easy to manage documents on your iPad"
date = "2012-12-03"
[taxonomies]
tags = ["stellarlibrary","website","ecommerce","python","flask",]
[extra]
client = "Stellar Software"
start = "2012-09-01"
end = "2013-02-01"
image = "/projects/stellarlibrary-home.png"
image_caption = "StellarLibrary"
+++


We worked with [StellarLibrary](http://www.stellarlibrary.com)
to extend the capabilities of their backend administration. 
<!-- more -->
![StellarLibrary](/projects/stellarlibrary-home.png)

These included a new Reseller capability,
and automatic foreign exchange conversion for invoices and receipts.

On the user-facing site, we implemented some routines that used ImageMagick to 
manipulate PDF document thumbnails, and customised folder images.

