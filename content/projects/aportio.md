+++
title = "Inboxagent"
date = "2024-01-01"
[taxonomies]
tags = ["aportio", "python", "django", "kubernetes", "machine-learning", "terraform", "htmx"]

[extra]
client = "Aportio"
start = "2020-09-01"
end = ""
+++

**Aportio**(https://aportio.com) provides a web-based automation tool named **InboxAgent**, 
which uses rules and AI to categorise incoming support desk emails, and raise support tickets.

<!-- more --> 
![Inboxagent](/projects/aportio.png)
I joined the company in 2020, in a full-time role as a Senior Developer.

During this time we have added major new features to the product, including
* background automated tasks to generate reports, create backups
* accepting requests from additional sources
  * direct API requests
  * reading requests files from S3 buckets
* using [HTMX](https://htmx.org/) to make dynamic pages (without having to make a client-side React or AngularJS app)

The classification process was overhauled to make it more flexible, and to extend the types of classification we could perform.

There has also been a large DevOps component to the role.
* migrating the application from AWS Beanstalk into a [Kubernetes](https://kubernetes.io/) infrastructure.
* performance optimisations for the database
* autoscaling the application and the infrastructure
* creating a [Terraform](https://www.terraform.io/) system to deploy new clusters
