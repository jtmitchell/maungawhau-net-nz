+++
title = "Convergence"
date = "2016-08-10"
[taxonomies]
tags = ["convergence","python"]
[extra]
client = "Convergence"
start = "2015-06-01"
end = "2015-11-01"
+++

With [Convergence,](http://www.convergence.co.nz/)
we worked with several clients to generate proposals for integrating the client's
inventory control systems with new e-commerce websites. Then we worked with the client
and their other vendors to create a customised solution to move inventory information
to the website, and web sales back to the head office.

<!-- more -->
