+++
title = "Time-of-Day Routing"
date = "2002-11-01"
[taxonomies]
tags = ["unl","linux","shell","backups",]
[extra]
client = "United Networks Ltd."
start = "2002-10-01"
+++


We performed "proof of concept" testing for a Linux-based solution to
implement new client services. 

<!-- more -->

The work involved gathering requirements and identifying the scope of the
testing; setting up a database and writing scripts to perform the
functions; and working through the test plan.

At the end of the tests we supplied the customer
with results and conclusions, requirements and 
design documents, and a tender for implementing the services on a Linux
server.
