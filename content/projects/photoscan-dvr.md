+++
title = "Photo-Scan DVR"
date = "2003-03-01"
[taxonomies]
tags = ["photoscan","linux","video",]
[extra]
client = "Photo-Scan Ltd."
start = "2003-02-01"
+++

We worked with Photo-Scan, who had started to import a Linux-based version of their DVR software.
The project was to customise the installation disk for the software, and provide their own defaults for various settings.
<!-- more -->

