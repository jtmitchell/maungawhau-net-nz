+++
title = "System Backups"
date = "2002-08-01"
[taxonomies]
tags = ["unl","linux","shell","backups",]
[extra]
client = "United Networks Ltd."
start = "2002-07-01"
+++


Installed and integrated software to provide an emergency re-installation
and recovery function for the network monitoring system. 
<!-- more -->

This involved the delivery of a CD-ROM that will automatically boot and re-install the 
original Linux system onto new hardware, and scripts to restore overnight
tape backups from a Solaris server on to the new system.

