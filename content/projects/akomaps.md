+++
title = "Akomaps"
slug = "akomaps"
date = "2022-03-20"
[taxonomies]
tags = ["typescript", "mongodb", "terraform"]
[extra]
client = "Cognitive Performance Labs"
start = "2020-02-01"
end = "2020-05-01"
+++

Worked on the production and scaling of the [Akomaps](https://akomaps.com) product.
Akomaps is a learning tool for note-taking and presentation in schools. It has the unique feature of linking notes to events on a video timeline.

<!-- more -->

I was involved in creating Terraform scripts to automate production deployments, and creating a CI/CD pipeline.
