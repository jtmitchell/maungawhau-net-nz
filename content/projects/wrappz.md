+++
title = "Wrappz"
description = "Customised skins for gadgets"
date = "2010-12-01"
[taxonomies]
tags = ["fujifilm","website","ecommerce","php","mysql",]
[extra]
client = "Fujifilm NZ"
start = "2009-12-01"
end = "2012-07-01"
image = "/projects/wrappz.co.nz-samples.png"
image_caption = "Wrappz samples"
+++


We worked on the Wrappz.co.nz project (using PHP and MySQL), to clean up code licensed from the U.K.
and implement the NZ website **www.wrappz.co.nz** 

<!-- more -->

Over about a month, we simplified the code, and implemented a single shopping cart
for the site. 

During 2010 we completely overhauled the backend administration,
changed the frontend to allow easy regional re-skinning of the site, and added
new features such as gift cards, commissions for artists.

![Wrappz giftcards](/projects/wrappz.co.nz-giftcards.png)

A skinned version of the site was deployed for the Australian market, and we had versions available for various other markets.

Here is a page showing an artist image. Notice how the sample in the lower right is wrapped onto the demo devices.
![Wrappz samples - notice the devices in the lower right.](/projects/wrappz.co.nz-samples.png)

Here are the pre-built covers for the iPhone 4. The image samples change when the selection is changed.
![Wrappz iPhones - the sample changes when the dropdown changes](/projects/wrappz.co.nz-iphone.png)

![Wrappz Artists](/projects/wrappz.co.nz-images.png)

