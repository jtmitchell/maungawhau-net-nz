+++
title = "Brace+"
slug = "braceplus"
date = "2022-03-20"
[taxonomies]
tags = ["usgboral", "typescript", "python", "django", "kubernetes"]
[extra]
client = "USG Boral"
start = "2019-10-01"
end = "2022-03-01"
+++

Provided support for the **Brace+** (www.braceplus.co.nz) web application.

<!-- more -->
**Brace+** provided customers with performance data about the USG Boral bracing products,
and a calculator to determine if the products would meet building standards for the size of structure being built.

The application ran as a React frontend and a Django backend with a Postgres database. All deployed on the Azure Kubernetes platform.

We worked under a support contract to keep the site updated, and add requested features.
