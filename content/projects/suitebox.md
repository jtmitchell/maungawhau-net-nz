+++
title = "Suitebox"
date = "2016-08-11"
[taxonomies]
tags = ["suitebox","website","php","angular",]
[extra]
client = "Suitebox"
start =  "2015-10-01"
end = "2015-11-01"
+++

We worked with [Suitebox](http://www.suitebox.com/)
on their unique video conferencing solution leading up to the initial product launch.

The focus was on fixing issues as they were identified, and automating the
Angular build process as well as the Jenkins CI deployment process.
<!-- more -->
