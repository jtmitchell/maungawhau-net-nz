+++
title = "Create A Poster"
description = "Team Posters for the AFL"
date = "2012-08-01"
[taxonomies]
tags = ["fujifilm","website","ecommerce","python","django",]
[extra]
client = "Fujifilm NZ"
start = "2009-12-01"
end = "2012-07-01"
image = "/projects/createaposter.com.au-cats.jpg"
image_caption = "Create A Poster"
+++

After starting with the wrappz.co.nz site, we continued working with Fujifilm NZ
to create new shopping sites.
<!-- more -->

Projects that went live are **Create A Poster** (http://afl.createaposter.com.au)
and **The Corner Dairy** (http://www.thecornerdairy.co.nz).

For **Create A Poster,** we needed to make the clubs pages use the club logo
colours.
* ![Club Page - background and logo vary according to the club](/projects/createaposter.com.au-suns.jpg)

Then we needed a dynamic way to show what poster you are buying as you select your customisable options.
* ![Custom Poster - initial page](/projects/createaposter.com.au-bartel.jpg)
* ![Custom Poster - choose a background](/projects/createaposter.com.au-bartel-bkg.jpg)
* ![Custom Poster - choose a style for the name](/projects/createaposter.com.au-bartel-name.jpg)
* ![Custom Poster - choose another combination](/projects/createaposter.com.au-bartel-other.jpg)


Projects that did not make it into production are a cartoon site, a chemicals site, and a book covers site.

These new projects were developed in Python + Django + Satchmo (providing
a pre-built shopping cart).

