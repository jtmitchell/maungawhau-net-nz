+++
title = "Woosh Network Monitoring"
date = "2004-07-01"
[taxonomies]
tags = ["woosh",".net","linux","network_monitoring","dns","soap",]
[extra]
client = "Woosh Wireless"
start = "2003-11-01"
end = "2004-06-01"
+++


We developed scripts and a database to store and graph SNMP data
from the IPWireless equipment. 

<!-- more -->

We also developed a Web services interface to the DNS system using Perl and
SOAP.
The interface allowed the DNS functions to be accessed from the
corporate Microsoft platform and through the website.
