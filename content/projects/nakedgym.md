+++
title = "The Naked Gym"
description = "An online store and social network for exercise"
date = "2009-09-01"
[taxonomies]
tags = ["consortium","website","python","django",]
[extra]
client = "Consortium"
start = "2009-04-01"
end = "2009-08-01"
image = "/projects/nakedgym-home.jpg"
image_caption = "The NakedGym"
+++


Worked on the nakedgym.co.nz site.
<!-- more -->
![The NakedGym](/projects/nakedgym-home.jpg)

The NakedGym was an online store built with Python and Django. Developing the actual
shopping cart functionality and integrating the system with the distributor's
warehousing and financial systems; and BAU activities such as fixing bugs,
writing new administration reports, and updating HTML content.

Provided occasional consultancy until January 2010
