+++
title = "Yellow Pages Refresh"
description = "Visual refresh of Yellow Pages website"
date = "2014-06-04"
[taxonomies]
tags = ["yellowpages","website","refresh","solr","python","django",]
[extra]
image = "/projects/yellow-homepage.jpg"
image_caption = "Yellow Homepage"
+++


[Yellow Pages](http://yellow.co.nz)
has had a visual refresh, and we have continued to improve the reviews 
features, and extend the user experience with photo uploads, bookmarking and saving searches.
<!-- more -->
![Yellow Homepage](/projects/yellow-homepage.jpg)

We have also spent time improving the search within the site, and
tagging business pages with Google-friendly metadata and schemas.

![Business listing](/projects/yellow-listing.jpg)

![Search resutls](/projects/yellow-search.png)
