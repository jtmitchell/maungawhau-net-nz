+++
title = "Suitebox again"
date = "2020-02-01"
[taxonomies]
tags = ["suitebox", "website","php","angular", "java", "docker", "aws-ecs", "terraform"]
[extra]
client = "Suitebox"
start = "2016-11-01"
end = "2020-02-01"
+++

Took up a full-time role at [Suitebox](http://www.suitebox.com/) as a Senior Developer.

<!-- more -->
![SuiteBox Document Signing](/projects/suitebox2-video-meeting.jpg)

During this period, we added features to the application:
* Implemented Digital Signing of PDF files.
* Allow users to add text and "Sign Here" markup for PDF files.
* Converted legacy electronic signing to a pure PDF version.
* Complete site redesign, including listing documents and signing locations
* Partner Integrations, including OAuth2 single sign-on
* Used Terraform to automate the provisioning of new instances, including moving between AWS regions
* Moved the Java backend out of OracleCloud into AWS, and wrote microservices to link to the PHP site
* Ran all microservices as Docker containers on Elastic Container Service

