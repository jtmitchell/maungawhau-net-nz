+++
title = "Yellow Pages"
date = "2013-03-04"
[taxonomies]
tags = ["yellowpages","website","python","django",]
[extra]
client = "Yellow Pages"
start = "2013-03-01"
end = "2015-03-01"
image = "/projects/yellowpages.jpg"
image_caption = "YellowPages"
+++


At **Yellow Pages**
we were involved in BAU defect fixes and rolling out the updated
versions of legacy sites.

The updates have included a visual refresh, adding ratings and reviews, photo uploads,
and social login with Facebook and Google+ on Yellow.co.nz

<!-- more -->
![YellowPages](/projects/yellowpages.jpg)
