+++
title = "Zola Static Site Generator"
description = "Moving from a custom build to Zola static-site generator."
date = "2024-01-15"
draft = false
[taxonomies]
tags = ["website","refresh",]
[extra]
share = true
comments = true
+++

Since 2015 this static site has been built from [Markdown](https://en.wikipedia.org/wiki/Markdown) files using a custom Javascript and [Gulp](https://gulpjs.com/) build process - described in the [Site Refresh](@/posts/site-refresh.md) post.
I have often wanted to move it over onto a "normal" static site generator tool, such as [Hugo](https://gohugo.io/), but was put off by the migration 
needed to change templates again, and make my layout and CSS work in a new theme system.

Then in the New Year down-time I found [Zola](https://www.getzola.org/).

<!-- more -->

Zola is a Rust static site generator, and uses the Jinja2 templating engine that I am comfortable with from this site, and working with Django.
So the migration of the templates was very easy.

It is set up to compile [Sass](https://sass-lang.com/) by default, so my CSS was used without change.

The biggest change, was to convert the markdown front-matter from YAML to TOML format, and that was only needed when I wanted to use the [Taxonomies](https://www.getzola.org/documentation/content/taxonomies/) feature.

So now the site is generated by Zola, and looks pretty much the same as before.
The next change will be to swap out the **Google Web Starter Kit** (which is not supported any more) for a supported CSS framework. 
At this point I am looking at either [Bulma](https://bulma.io/), [Tailwind](https://tailwindcss.com/), or the old warhorse [Bootstrap](https://getbootstrap.com/).
