+++
title = "What happened to PaaS?"
description = "Second thoughts about cloud deployment"
date = "2023-02-11"
draft = false
[taxonomies]
tags = ["cloud","paas","cloud-foundry","docker", "kubernetes",]
[extra]
share = true
comments = true
+++

Back in 2014 [I was very excited](@/posts/what-cloud-means-to-me.md) about [Platform as a Service (PaaS)](http://en.wikipedia.org/wiki/Platform_as_a_service), and especially [Cloud Foundry](https://www.cloudfoundry.org/).

Then along came [Docker](https://www.docker.com/) and [Kubernetes](https://kubernetes.io/)...
<!-- more -->

Cloud Foundry, like Heroku, is still out there, and still something you can choose to use. But developers aren't...

Here is my super-late, hot take. The killer feature of using build-packs to automatically build and run your app, lost out to writing Dockerfiles and running apps in containers. And PaaS platforms did not offer the flexibility for managing and scaling the cloud resources that Kubernetes offers.
So instead of the simplicity of pushing code to the platform, magic happening, then your application is running, we have learnt to write the Dockerfiles, build the container images, and deploy them with container orchestration systems like Kubernetes - and yes, Cloud Foundry runs apps in containers as well.

One key benefit of Docker's approach is that developers can incrementally build up the container. One group of developers can start with a standard operating system container, and add in the libraries and source to build and run their application. Then another group of developers can ***extend*** that existing container with a different configuration, more tools, or whatever they want.

Finally, the container orchestration systems allow us to group containers together as a service - they can be treated almost like Lego blocks, just start the container and let it run. Containers that run Redis, or a database like MySQL or PostgreSQL may need little more configuration than adding access credentials.
