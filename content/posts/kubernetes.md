+++
title = "Why Kubernetes"
slug = "why-kubernetes"
description = "Why we moved to Kubernetes for container orchestration."
date = "2023-12-01"
draft = false
[taxonomies]
tags = ["cloud","kubernetes", "microk8s", "k3s", "k0s", "aws-eks", "prometheus", "grafana", "karpenter"]
[extra]
share = true
comments = true
+++

[Kubernetes](https://kubernetes.io) has taken control of my (working) life.

<!-- more -->

When working for [Suitebox](@/projects/suitebox-again.md) we moved the backend Java services out of OracleCloud and into the AWS cloud.
We also created some new services, PDF digital signing, Keycloak IDP, and a Spring Configuration Server.
All of these services were built as Docker images, and when deciding how to run them, I asked "***Will we need to run these in multiple clouds?***". The answer was "***No***", which strongly indicated we didn't need the complexity of Kubernetes. Instead, we ran the services on the [AWS Elastic Container Service](https://aws.amazon.com/ecs/).

When it came time to ask the same question at [Aportio](@/projects/aportio.md), the answer was "***Probably***". So we committed to moving away from [AWS Elastic Beanstalk](https://aws.amazon.com/elasticbeanstalk/) and into the [AWS Elastic Kubernetes Service](https://aws.amazon.com/eks/). The idea is that in the future we can target any cloud provider that offers a managed Kubernetes service - or even run the application on our own hardware in a data centre using a Kubernetes distribution such as [MicroK8s](https://microk8s.io/), [K3s](https://k3s.io/), and [k0s](https://docs.k0sproject.io/).

So why Kubernetes? For the ability to run our workloads on any cloud or platform. The ability to use a managed version of Kubernetes, with the cloud provider managing the control plane, made it much faster to get the project up and running.

Once we were in the Kubernetes environment, I appreciated the ability to run [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/) to see how the nodes and applications are performing; and the Kubernetes superpower, autoscaling the application based on load, and restarting failed pods. 

I also want to show some love to [Karpenter](https://karpenter.sh/), which provisions new nodes when the workload expands. It only supports AWS (in 2023) but aims to support other clouds as well. The promise it makes is to be aware of how much resource you require, and to be aware of the costs for different virtual machines in the cloud, then to provision a new node base on the cheapest node to meet the demand. It is also able to re-assess the current resources demands, then consolidate the running pods onto a cheaper set of nodes.
