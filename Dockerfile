FROM ghcr.io/getzola/zola:v0.18.0 as zola

COPY . /site
WORKDIR /site
RUN ["zola", "build"]

FROM ghcr.io/static-web-server/static-web-server:2
WORKDIR /
COPY --from=zola /site/public /public
