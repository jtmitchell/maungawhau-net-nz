# [Maungawhau Information Technology](http://www.maungawhau.net.nz)

[![Build Status](https://gitlab.com/jtmitchell/maungawhau-net-nz/badges/master/pipeline.svg)](https://gitlab.com/jtmitchell/maungawhau-net-nz/)

## Overview

This is the source for the [Maungawhau IT](http://www.maungawhau.net.nz) static website. 
The site is generated from markdown files using [Zola](https://www.getzola.org/), 
and uploaded to an AWS S3 bucket for serving to the web.


## Inspiration

The base HTML is based on [Web Starter Kit](https://github.com/google/web-starter-kit/releases/latest), 
using the Rust based [Zola](https://www.getzola.org/) static site generator.

* This originally used [Hyde](http://hyde.github.io/) to generate the static site.
* Then a custom JS workflow inspired by Sean Farrell and Daniel Naab's excellent posts (and code) about using gulp to transform markdown into static HTML pages.

  * [Sean's post](http://www.rioki.org/2014/06/09/jekyll-to-gulp.html) and [repository](https://github.com/rioki/www.rioki.org)
  * [Daniel's post](http://blog.crushingpennies.com/a-static-site-generator-with-gulp-proseio-and-travis-ci.html) and 
[repository](https://github.com/danielnaab/wunderdog/)


## Licence

* The content and images for the site are copyright Maungawhau Information Technology Ltd.
* The code for the site is licensed under the Apache 2.0 licence
* Web Starter Kit [licence information](https://github.com/google/web-starter-kit/blob/master/LICENSE).
Web Starter Kit is copyright Google Inc
